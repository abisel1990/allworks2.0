<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Request as Req;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\DB;
use App\Models\Empleos;
use App\Models\Postulante;


class BuscarEmpleoController extends Controller
{
    public function buscar(Request $request){
        $search = $request->search;

        if($search == ''){
           $employees =$employees = DB::table('empleos')->
           join('tipos_empleos','empleos.id_empleos','=','tipos_empleos.id')
           ->select('empleos.id','tipos_empleos.descripcion as desc')
           ->orderBy('desc','asc')
           ->get();
        }else{
           $employees = DB::table('empleos')->
           join('tipos_empleos','empleos.id_empleos','=','tipos_empleos.id')
           ->select('empleos.id','tipos_empleos.descripcion as desc')
           ->where('tipos_empleos.descripcion','like', '%'.$search.'%')
           ->orderBy('desc','asc')
           ->get();
        }

        $response = array();
        foreach($employees as $employee){
           $response[] = array(
                "id"=>$employee->id,
                "text"=>$employee->desc
           );
        }

        echo json_encode($response);
        exit;

    }
    public function buscarpor(Request $request){
        $search = $request->search;
        $clientes = DB::table('empleos')->
        join('tipos_empleos','empleos.id_empleos','=','tipos_empleos.id')
        ->select('empleos.id','tipos_empleos.descripcion as desc')
        ->where('tipos_empleos.descripcion','like', '%'.$search.'%')
        ->orderBy('desc','asc')
        ->get();

        return view('buscar',compact('clientes'));
    }
    public function buscarempleo(Request $request){
        $search = $request->search;

        $id = auth()->user()->id;
        $postulante = Postulante::whereHas('user', function ($query) use ($id) {
            return $query->where('id_usuario', '=', $id);
        })->get();
		$clientes = DB::table('empleos')->
        join('tipos_empleos','empleos.id_empleos','=','tipos_empleos.id')
        ->join('empresas','empleos.id_empresa','=','empresas.id')
        ->select('empleos.id','empleos.salario','empleos.descripcion as empdesc'.'tipos_empleos.descripcion as descr','empresas.nombre_empresa','empresas.foto','empresas.direccion'
        ,'empresas.estado','empresas.pais','empresas.ciudad')
        ->where('tipos_empleos.descripcion','like', '%'.$search.'%')
        
        ->get();
        return view('buscar',compact('clientes','postulante'));
    }
}
