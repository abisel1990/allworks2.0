<?php

namespace App\Http\Controllers\Empresa;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Empleos;
use App\Models\Empresa;
use App\Models\TipoEmpleo;
use App\Models\Postulacion;

class EmpleoController extends Controller
{
    public function __construct()
    {
        $this->middleware('empresa');
    }

    public function crearEmpleo()
    {
        $tipos_empleos = TipoEmpleo::get();
        return view('Empresa/crearEmpleo',compact('tipos_empleos'));
    }
    public function store(Request $request){
        // return $request->nombrePuesto;

        if(auth()->user()->id_rol == 1 ){
            $id = \auth()->user()->id;
        $empresa = Empresa::whereHas('user', function ($query) use ($id) {
            return $query->where('id_usuario', '=', $id);
        })->get();
        $json = json_decode(\json_encode($empresa),true);
        $id_empresa = 0;
        foreach($json as $js){
            $id_empresa = $js['id'];
        }    
           
        $rules = array(
            'nombrePuesto'           => 'required',
            'descripcion'  => 'required',
            'salario'  => 'required',
         
        );
        $this->validate($request, $rules);
        
        $empleos = new Empleos();
        $empleos->id_empresa = $id_empresa;
        $empleos->id_empleos = $request->nombrePuesto;
        $empleos->descripcion = $request->descripcion;
        $empleos->salario = $request->salario;
        $empleos->save();

        return redirect()->route('empleo');

        }else {
            return response()->json([
                'error'=>'No tienes los permisos'
            ]);
        }

    }

    public function show($id){
        $id_user = auth()->user()->id;
      
        // $postulantes = Postulante::where('id_usuario',$id_user)->pluck('id');
        // $id_postulante = 0;
        // foreach($postulante as $po){
        //     $id_postulante = $po;
        // }

        $mis_postulaciones = Postulacion::whereHas('empleado', function ($query) use ($id) {
            return $query->where('id_empleo', '=', $id);
        })->
        whereHas('empresa', function ($query) use ($id_user) {
            return $query->where('id_usuario', '=', $id_user);
        })
        ->with('postulante',)->get();
        // return $mis_postulaciones;
        // return \view('PostulanEte/index',compact('mis_postulaciones'));
        return view('Empresa/index',compact('mis_postulaciones'));

    }

}
