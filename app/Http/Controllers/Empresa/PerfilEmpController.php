<?php

namespace App\Http\Controllers\Empresa;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Empresa;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class PerfilEmpController extends Controller
{
    public function __construct()
    {
        $this->middleware('empresa');
    }

    public function perfil()
    {
        if (\auth()->user()->id_rol == 1) {

            $id = auth()->user()->id;
            $empresa = Empresa::whereHas('user', function ($query) use ($id) {
                return $query->where('id_usuario', '=', $id);
            })->get();

            return view('Empresa/perfil', \compact('empresa'));
        } else {
            return response()->json([
                'Error' => 'No tienes los permisos para acceder a los datos'
            ]);
        }
    }
    public function registro()
    {
        if (\auth()->user()->id_rol == 1) {

            $id = auth()->user()->id;
            $empresa = Empresa::whereHas('user', function ($query) use ($id) {
                return $query->where('id_usuario', '=', $id);
            })->get();
            $json = \json_decode(\json_encode($empresa), true);
            $jss = 0;
            foreach ($json as $js) {
                $jss = $js;
            }
            if ($jss['razon_social'] == null || $jss['RFC'] == null || $jss['giro_empresarial'] == null) {

                return view('Empresa/partials/registro');
            } else {
                return redirect()->route('perfil');
            }
        } else {
            return response()->json([
                'Error' => 'No tienes los permisos para acceder a los datos'
            ]);
        }
    }
    public function store(Request $request)
    {
        if (\auth()->user()->id_rol == 1) {
            $id = auth()->user()->id;
            $empresa = Empresa::whereHas('user', function ($query) use ($id) {
                return $query->where('id_usuario', '=', $id);
            });
            $data = request()->validate([

                'imagen' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
            ]);
            $file = $request->file('imagen');
            $filename = 'empresa-';
            $filename .= Str::random(15);
            $filename .= '-';
            $filename  .= $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $imagen   = $filename;
            $file->move(public_path('imagenes/empresa'), $imagen);
            $url = 'http://localhost:8000/imagenes/empresa/';
            $url .= $filename;
            $empresa->update(
                [   'nombre_empresa'=>$request->nombre_empresa,
                    'razon_social' => $request->razon_social,
                    'RFC' => $request->RFC,
                    'pais' => $request->pais,
                    'estado' => $request->estado,
                    'ciudad' => $request->ciudad,
                    'direccion' => $request->direccion,
                    'giro_empresarial' => $request->giro_empresarial,
                    'descripcion' => $request->descripcion,
                    'pagina_web' => $request->pagina_web,
                    'cargo' => $request->cargo,
                    'foto' => $url,
                    'telefono' => $request->telefono,
                ]
            );

            return redirect()->route('perfil');
        } else {
            return response()->json([
                'Error' => 'No tienes los permisos para acceder a los datos'
            ]);
        }
    }
    public function edit(Empresa $empresa)
    {
        $id = auth()->user()->id;

        $empresa = Empresa::whereHas('user', function ($query) use ($id) {
            return $query->where('id_usuario', '=', $id);
        })->get();
        return view('Empresa/partials/editar', compact('empresa'));
    }
    public function actualizar(Request $request)
    {

        $id = auth()->user()->id;
        $empresa = Empresa::whereHas('user', function ($query) use ($id) {
            return $query->where('id_usuario', '=', $id);
        });
        $url = '';

      

        $foto =  Empresa::whereHas('user',function($query) use($id){
            return $query->where('id_usuario','=',$id);
         })->get();
         $ur = '';
        $json = json_decode(\json_encode($foto),true);
        foreach($json as $imagenbd){
       
            $url = $imagenbd['foto'];
        }
        
       
        
        if($request->imagen){
            
                    $u = explode('/',$url);
                    $ur = $u[5];
               
            Storage::delete('imagenes/empresa/'.$ur);

            $data = request()->validate([

                'imagen' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
            ]);
            $file = $request->file('imagen');
            $filename = 'empresa-';

            $filename .= Str::random(15);
            $filename .= '-';
            $filename  .= $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $imagen   = $filename;
            $file->move(public_path('imagenes/empresa'), $imagen);
            $url = 'http://localhost:8000/imagenes/empresa/';
            $url .= $filename;
        } else {
            $foto =  Empresa::whereHas('user', function ($query) use ($id) {
                return $query->where('id_usuario', '=', $id);
            })->get();
            $json = json_decode(\json_encode($foto), true);
            foreach ($json as $imagenbd) {
                $url = $imagenbd['foto'];
            }
        }

        $empresa->update(
            [
                'nombre_empresa'=>$request->nombre_empresa,
                'razon_social' => $request->razon_social,
                'RFC' => $request->RFC,
                'pais' => $request->pais,
                'estado' => $request->estado,
                'ciudad' => $request->ciudad,
                'direccion' => $request->direccion,
                'giro_empresarial' => $request->giro_empresarial,
                'descripcion' => $request->descripcion,
                'pagina_web' => $request->pagina_web,
                'cargo' => $request->cargo,
                'foto' => $url,
                'telefono' => $request->telefono,
            ]
        );
         return redirect()->route('perfil');



    }
}
