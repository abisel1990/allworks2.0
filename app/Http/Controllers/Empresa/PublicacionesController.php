<?php

namespace App\Http\Controllers\Empresa;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Empleos;
use App\Models\Empresa;

class PublicacionesController extends Controller
{
    public function __construct()
    {
        $this->middleware('empresa');
    }

    public function Publicaciones()
    {
        $id = \auth()->user()->id;

        $empresa = Empresa::whereHas('user', function ($query) use ($id) {
            return $query->where('id_usuario', '=', $id);
        })->get();
        $json = json_decode(\json_encode($empresa),true);
        $id_empresa = 0;
        foreach($json as $js){
            $id_empresa = $js['id'];
        }
        $empleo = Empleos::whereHas('empleado', function ($query) use ($id_empresa) {
            return $query->where('id_empresa', '=', $id_empresa);
        })->get();
        // $datos ['empleos']=Empleos::paginate(5);

        return view('Empresa/publicaciones',compact('empleo'));


    }

    public function destroy($id)
    {
        $note = Empleos::find($id);
        $note->delete();
        return redirect('/');
    }
}
