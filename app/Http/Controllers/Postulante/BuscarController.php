<?php

namespace App\Http\Controllers\Postulante;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BuscarController extends Controller
{
    public function __construct()
    {
        $this->middleware('postulante');
    }

    public function index()
    {
        return view('Postulante/buscarEmpleo');
    }
}
