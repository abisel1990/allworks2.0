<?php

namespace App\Http\Controllers\Postulante;

use App\Http\Controllers\Controller;
use App\Models\Postulante;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class PerfilPosController extends Controller
{
    public function __construct()
    {
        $this->middleware('postulante');
    }
    public function perfil()
    {
        if (\auth()->user()->id_rol == 2) {

            $id = auth()->user()->id;
            $postulante = Postulante::whereHas('user', function ($query) use ($id) {
                return $query->where('id_usuario', '=', $id);
            })->get();

            return view('Postulante/perfil', \compact('postulante'));
        } else {
            return response()->json([
                'Error' => 'No tienes los permisos para acceder a los datos'
            ]);
        }
    }

    public function registro()
    {
        if (\auth()->user()->id_rol == 2) {

            $id = auth()->user()->id;
            $postulante = Postulante::whereHas('user', function ($query) use ($id) {
                return $query->where('id_usuario', '=', $id);
            })->get();
            $json = \json_decode(\json_encode($postulante), true);
            $jss = 0;
            foreach ($json as $js) {
                $jss = $js;
            }
            if ($jss['genero'] == null || $jss['estado_civil'] == null || $jss['cargo'] == null) {

                return view('Postulante/partials/registro');
            } else {
                return redirect()->route('perfilPostulante');
            }
        } else {
            return response()->json([
                'Error' => 'No tienes los permisos para acceder a los datos'
            ]);
        }
    }

    public function store(Request $request)
    {
        if (\auth()->user()->id_rol == 2) {
            $id = auth()->user()->id;
            $postulante = Postulante::whereHas('user', function ($query) use ($id) {
                return $query->where('id_usuario', '=', $id);
            });
            $data = request()->validate([

                'imagen' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
            ]);
            $file = $request->file('imagen');
            $filename = 'postulante-';
            $filename .= Str::random(15);
            $filename .= '-';
            $filename  .= $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $imagen   = $filename;
            $file->move(public_path('imagenes/postulante'), $imagen);
            $url = 'http://localhost:8000/imagenes/postulante/';
            $url .= $filename;
            $postulante->update(
                [
                    'fecha_nacimiento' => $request->fecha_nacimiento,
                    'genero' => $request->genero,
                    'estado_civil' => $request->estado_civil,
                    'estado' => $request->estado,
                    'telefono' => $request->telefono,
                    'pais' => $request->pais,
                    'estado' => $request->estado,
                    'ciudad' => $request->ciudad,
                    'nacionalidad' => $request->nacionalidad,
                    'discapacidad' => $request->discapacidad,
                    'foto' => $url,
                    'cargo' => $request->cargo,
                    'descripcion_cargo' => $request->descripcion_cargo,
                ]
            );

            return redirect()->route('perfilPostulante');
        } else {
            return response()->json([
                'Error' => 'No tienes los permisos para acceder a los datos'
            ]);
        }
    }

    public function edit(Postulante $postulante)
    {
        $id = auth()->user()->id;

        $postulante = Postulante::whereHas('user', function ($query) use ($id) {
            return $query->where('id_usuario', '=', $id);
        })->get();
        return view('Postulante/partials/editar', compact('postulante'));
    }

    public function actualizar(Request $request)
    {

        $id = auth()->user()->id;
        $postulante = Postulante::whereHas('user', function ($query) use ($id) {
            return $query->where('id_usuario', '=', $id);
        });
        $url = '';
        $foto =  Postulante::whereHas('user',function($query) use($id){
            return $query->where('id_usuario','=',$id);
         })->get();
         $ur = '';
        $json = json_decode(\json_encode($foto),true);
        foreach($json as $imagenbd){
           if($request->imagen){
            if($imagenbd['foto']){
                $u = explode('/',$url);
                $ur = $u[5];
            }
           }
            $url = $imagenbd['foto'];
        }
        if ($request->imagen) {
            Storage::delete('imagenes/postulante/'.$ur);

            $data = request()->validate([

                'imagen' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
            ]);
            $file = $request->file('imagen');
            $filename = 'postulante-';
            $filename .= Str::random(15);
            $filename .= '-';
            $filename  .= $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $imagen   = $filename;
            $file->move(public_path('imagenes/postulante'), $imagen);
            $url = 'http://localhost:8000/imagenes/postulante/';
            $url .= $filename;
        } else {
            $foto =  Postulante::whereHas('user', function ($query) use ($id) {
                return $query->where('id_usuario', '=', $id);
            })->get();
            $json = json_decode(\json_encode($foto), true);
            foreach ($json as $imagenbd) {
                $url = $imagenbd['foto'];
            }
        }
        var_dump(explode('/', $url, 5));

        $postulante->update(
            [
                'fecha_nacimiento' => $request->fecha_nacimiento,
                'genero' => $request->genero,
                'estado_civil' => $request->estado_civil,
                'estado' => $request->estado,
                'telefono' => $request->telefono,
                'pais' => $request->pais,
                'estado' => $request->estado,
                'ciudad' => $request->ciudad,
                'nacionalidad' => $request->nacionalidad,
                'discapacidad' => $request->discapacidad,
                'foto' => $url,
                'cargo' => $request->cargo,
                'descripcion_cargo' => $request->descripcion_cargo,
            ]
        );
    }
}
