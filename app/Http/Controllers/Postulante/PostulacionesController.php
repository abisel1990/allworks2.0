<?php

namespace App\Http\Controllers\Postulante;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Empleos;
use App\Models\Empresa;
use App\Models\Postulante;
use App\Models\Postulacion;
class PostulacionesController extends Controller
{

    public function __construct()
    {
        $this->middleware('postulante');
    }
    
    public function postulaciones()
    {
        $id = auth()->user()->id;
        $postulante = Postulante::whereHas('user', function ($query) use ($id) {
            return $query->where('id_usuario', '=', $id);
        })->get();
        $empleos = Empleos::with('empleado','empresa')->get();
        // $json = json_decode(\json_encode($empleos),true);
        // foreach($json as $empleo){
        //     return $empleo;
        // }
        return view('Postulante/buscarEmpleo',compact('empleos','postulante'));

    }
    public function postularme($id, Empresa $empresa){
        $id_user = auth()->user()->id;
      
        $postulante = Postulante::where('id_usuario',$id_user)->pluck('id');
       
        $id_postulante = 0;
        foreach($postulante as $po){
            $id_postulante = $po;
        }
        $postularme = new Postulacion;
        $postularme->id_empresa = $empresa->id;
        $postularme->id_empleo = $id;
        $postularme->id_postulante = $id_postulante;
        $postularme->status = 'En proceso';
        $postularme->save();

        return \redirect()->route('empleos');

        
        
    }
}
