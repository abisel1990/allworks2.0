<?php

namespace App\Http\Controllers\Postulante;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Postulante;
use App\Models\Postulacion;
class PostulanteController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('postulante');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $id_user = auth()->user()->id;
        $postulante = Postulante::where('id_usuario',$id_user)->pluck('id');
        $id_postulante = 0;
        foreach($postulante as $po){
            $id_postulante = $po;
        }
        $mis_postulaciones = Postulacion::whereHas('postulante', function ($query) use ($id_postulante) {
            return $query->where('id_postulante', '=', $id_postulante);
        })->with('empresa','empleado',)->get();
        return \view('Postulante/index',compact('mis_postulaciones'));
    }

    public function contactoPos()
    {
        return view('contacto');
    }
}
