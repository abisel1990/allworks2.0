<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class PostulanteMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(!auth()->check())
        return redirect('login');

        
        if(auth()->user()->id_rol == 1){
            return redirect('empleo');
        }
        
        return $next($request);
    }
}
