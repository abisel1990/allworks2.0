<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\TipoEmpleo;
class Empleos extends Model
{
    use HasFactory;

    public function empleado(){
        return $this->hasOne(TipoEmpleo::class, 'id','id_empleos');
        
    }
    public function empresa(){
        return $this->hasOne(empresa::class, 'id','id_empresa');
        
    }

}
 