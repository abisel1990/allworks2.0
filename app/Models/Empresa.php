<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;
class Empresa extends Model
{
    use HasFactory;
    protected $fillable = [
        'id_usuario',
        'razon_social',
        'RFC',
        'pais',
        'estado',
        'ciudad',
        'direccion',
        'giro_empresarial',
        'descripcion',
        'pagina_web',
        'foto',
        'cargo',
        'telefono',
    ];
    public function user(){
        return $this->hasOne(user::class,'id','id_usuario');
    }

}
