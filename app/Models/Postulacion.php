<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Postulacion extends Model
{
    use HasFactory;
    public function empleado(){
        return $this->hasOne(Empleos::class, 'id','id_empleo');
        
    }
    public function empresa(){
        return $this->hasOne(Empresa::class, 'id','id_empresa');
        
    }
    public function postulante(){
        return $this->hasOne(Postulante::class, 'id','id_postulante');
        
    }
}
