<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Postulante extends Model
{
    use HasFactory;
    protected $fillable = [
        'id_usuario',
        'fecha_nacimiento',
        'genero',
        'estado_civil',
        'telefono',
        'pais',
        'estado',
        'ciudad',
        'nacionalidad',
        'discapacidad',
        'cargo',
        'descripcion_cargo',
        'foto',
    ];
    public function user(){
        return $this->hasOne(user::class,'id','id_usuario');
    }
}
