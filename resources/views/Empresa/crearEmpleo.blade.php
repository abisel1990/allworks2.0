@extends('layouts.nav')

@section('content')

<style>

 .form-control {
        border-radius: 15px;
        font-family: 'Roboto', sans-serif;
        color: #17161C;
       
        border: 0.5px solid #ced4da;
    }
.btn{
  color: white;
  background-color: #2255FF;
  width: 30%;
  border-radius: 30px;
 
}
.btn:hover{
  background-color: #002CBF;
  color: white;
  }
</style>
<br><br><br><br>
<div class="container">
  <div class="row">
    <div class="col-sm-12">
      <h2>Publicar Nueva vacante</h2>
    </div>
  </div>
</div>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
                <div class="card-body">
                    <form action="{{url('guardar')}}" method="POST" enctype="multipart/form-data">
                            @csrf
                        <div class="row marge-top">
                            <div class="col-md-3">
                              <h5>Puesto:</h5>
                            </div>
                            <div class="col-md-5 text-center">
                              <div class="form-group">
                              
                                <select class="form-control" name="nombrePuesto" id="">
                              @foreach($tipos_empleos as $tipo)
                                  <option value="{{$tipo->id}}">{{$tipo->descripcion}}</option>
                              @endforeach
                                </select>
                                <!-- <input type="text" name="nombrePuesto" placeholder="Ejemplo: Cocinero" id="nombrePuesto" > -->
                              </div>
                            </div>
                          </div>

                          <div class="row marge-top">
                            <div class="col-md-3">
                              <h5>Descipción:</h5>
                            </div>
                            <div class="col-md-5">
                              <div class="form-group">
                              <textarea class="form-control" name="descripcion" rows="10" cols="50" placeholder="Descripción del puesto"></textarea>
                              </div>
                            </div>
                          </div>
                          
<!-- poner en la bd el campo de requisitos -->
                          <div class="row marge-top">
                            <div class="col-md-3">
                              <h5>Requisitos:</h5>
                            </div>
                            <div class="col-md-5">
                              <div class="form-group">
                              <textarea class="form-control" name="requisitos" rows="5" cols="50" placeholder="Requisitos del puesto"></textarea>
                              </div>
                            </div>
                          </div>

                        <div class="row marge-top">
                            <div class="col-md-3">
                              <h5>Salario:</h5>
                            </div>
                            <div class="col-md-5">
                              <div class="form-group">
                                <input class="form-control" type="number" name="salario" placeholder="Ejemplo: $2000" id="salario" >
                              </div>
                            </div>
                          </div>
                          
                          <br>
                          <div class="col-md-11 text-center">
                          <button type="submit" value="guardar" class="btn rounded-pill">
                                    {{ __('Publicar') }}
                          </button>
                          </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection






