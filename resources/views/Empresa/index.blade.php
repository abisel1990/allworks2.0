@extends('layouts.nav')

@section('content')
<style>
.card{
    border-radius: 25px; 
}
.card-img-top{
border-radius:100%;
}
</style>


<div class="container">
    <div class="row">
     @foreach($mis_postulaciones as $postulantes)
        <div class="col-md-4">
            <div class="card ">
                <div class="card-body text-center">
                    <img class="card-img-top " src="https://i.ibb.co/MpNDbbH/fd2678b89102df618557e24c9a00e350.jpg" alt="abisel" style="width: 50%;">
                </div>
                    <div class="card-body">
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item"><p class="card-text">Puesto: {{ $postulantes->empleado->empleado->descripcion}}</p></li>
                            <li class="list-group-item"><p class="card-text">Nombre: {{$postulantes->postulante->user->name}}</p></li>
                            <li class="list-group-item"><p class="card-text">Sexo: {{ $postulantes->empleado->empleado->descripcion}}</p></li>
                            <li class="list-group-item"><p class="card-text">Discpapacidad: {{ $postulantes->empleado->empleado->descripcion}}</p></li>
                            <li class="list-group-item"><p class="card-text">Telefono: {{$postulantes->postulante->telefono}}</p></li>
                        </ul>
                    </div>
            </div>
        </div>
     @endforeach
    </div>
</div>
@endsection
