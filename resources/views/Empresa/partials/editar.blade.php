@extends('layouts.nav')

@section('estilos')
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.css" integrity="sha512-In/+MILhf6UMDJU4ZhDL0R0fEpsp4D3Le23m6+ujDWXwl3whwpucJG1PEmI3B07nyJx+875ccs+yX2CqQJUxUw==" crossorigin="anonymous" />
@endsection

@section('content')
<style>

.btn{
  color: white;
  background-color: #2255FF;
  width: 15%;
  border-radius: 30px;
 
}
.btn:hover{
  background-color: #002CBF;
  color: white;
}
.navbtn{
    color: #2255FF;
    border-color: #2255FF;
    border: solid 1px;
    background-color: white;
    border-radius: 30px;
    font-weight: bold;
    width: 15%; 
}
.navbtn:hover{
    background-color: #2255FF;
    color: white;
}
.form-control{
    border-radius: 15px;
    font-family: 'Roboto', sans-serif;
    color: #17161C;
    border: 0.5px solid #ced4da;
}
</style>
           @foreach($empresa as $emp)
           <div class="col-md-12 mx-auto bg-white p-3">
               <form action="{{ route('perfil.actualizar') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="row">
                <div class="form-group col-md-4">
                            <label for="nombre_empresa">Nombre de la empresa*</label>
                            <input id="nombre_empresa" class="text-uppercase form-control @error('rezon_social') is-invalid @enderror" type="text" 
                        name="nombre_empresa" value="{{$emp->nombre_empresa}}" />
                            @error('nombre_empresa')
                        <div class="invalid-feedback d-block" role="alert">{{$message}}</div>
                            @enderror
                        </div>
                        <div class="form-group col-md-4">
                            <label for="razon_social">Razón social*</label>
                            
                            <input id="razon_social" class="form-control @error('rezon_social') is-invalid @enderror" type="text" 
                        name="razon_social" value="{{$emp->razon_social}}" />
                            @error('razon_social')
                        <div class="invalid-feedback d-block" role="alert">{{$message}}</div>
                            @enderror
                        </div>
                        
                        <div class="form-group col-md-4">
                            <label for="RFC">RFC*</label>
                            <input id="RFC" class=" text-uppercase form-control @error('RFC') is-invalid @enderror" type="text" 
                        name="RFC" value="{{$emp->RFC}}" />
                            @error('RFC')
                        <div class="invalid-feedback d-block" role="alert">{{$message}}</div>
                            @enderror
                        </div>
                        <div class="form-group col-md-4">
                            <label for="pais">País*</label>
                            <select id="pais" class="form-control @error('pais') is-invalid @enderror" type="text" name="pais" value="{{ old('pais')}}" >
                        <option>México</option>
                        </select>
                            @error('pais')
                        <div class="invalid-feedback d-block" role="alert">{{$message}}</div>
                            @enderror
                        </div>
                        <div class="form-group col-md-4">
                            <label for="estado">Estado*</label>
                            <select id="estado" class="form-control @error('estado') is-invalid @enderror" type="text" name="estado" value="{{ old('estado')}}" >
                        <option>Campeche</option>
                        <option>Quintana Roo</option>
                        <option>Yucantán</option>
                        </select>
                            @error('estado')
                        <div class="invalid-feedback d-block" role="alert">{{$message}}</div>
                            @enderror
                        </div>
                        <div class="form-group col-md-4">
                            <label for="ciudad">Ciudad*</label>
                            <input id="ciudad" class="form-control @error('ciudad') is-invalid @enderror" type="text" 
                        name="ciudad" value="{{$emp->ciudad}}" />
                            @error('ciudad')
                        <div class="invalid-feedback d-block" role="alert">{{$message}}</div>
                            @enderror
                        </div>
                        <div class="form-group col-md-4">
                            <label for="direccion">Dirección*</label>
                            <input id="direccion" class="form-control @error('direccion') is-invalid @enderror" type="text" 
                        name="direccion" value="{{$emp->direccion}}" />
                            @error('direccion')
                        <div class="invalid-feedback d-block" role="alert">{{$message}}</div>
                            @enderror
                        </div>
                        
                       
                </div>
                <div class="form-group">
                    <label for="descripcion">Descripción*</label>
                    <textarea id="descripcion" class="form-control @error('descripcion') is-invalid @enderror" name="descripcion">{{$emp->descripcion}}</textarea>
                    @error('descripcion')
                    <div class="invalid-feedback d-block" role="alert">{{$message}}</div>
                        @enderror
                </div>
                <div class="row">
                <div class="form-group col-md-4">
                            <label for="giro_empresarial">Giro empresarial*</label>
                            <input id="giro_empresarial" class="form-control @error('giro_empresarial') is-invalid @enderror" type="text" 
                        name="giro_empresarial" value="{{$emp->giro_empresarial}}" />
                            @error('giro_empresarial')
                        <div class="invalid-feedback d-block" role="alert">{{$message}}</div>
                            @enderror
                        </div>
                    <div class="form-group col-md-6">
                        <label for="pagina_web">Página web*</label>
                        <input id="pagina_web" class="form-control @error('pagina_web') is-invalid @enderror" type="text" 
                    name="pagina_web" value="{{$emp->pagina_web}}" />
                        @error('pagina_web')
                    <div class="invalid-feedback d-block" role="alert">{{$message}}</div>
                        @enderror
                    </div>
                    <div class="form-group col-md-6">
                        <label for="cargo">Cargo de quién se suscribe*</label>
                        <select name="cargo" id="id_rol" class="form-control @error('id_rol') is-invalid @enderror" required  value="{{$emp->cargo}}">
                        
                                    <option value="{{$emp->cargo}}">{{$emp->cargo}}</option>
                                    <option value="Administrador">Administrador</option>
                                    <option value="Gerente">Gerente</option>
                                    <option value="Director">Director</option>
                                    <option value="Recursos humanos">Recursos humanos</option>
                        </select>
                      
                        @error('cargo')
                    <div class="invalid-feedback d-block" role="alert">{{$message}}</div>
                        @enderror
                    </div>
                    <div class="form-group col-md-6">
                        <label for="telefono">Teléfono*</label>
                        <input id="telefono" class="form-control @error('telefono') is-invalid @enderror" type="number" step="0.01"
                    name="telefono" value="{{$emp->telefono}}" />
                        @error('telefono')
                    <div class="invalid-feedback d-block" role="alert">{{$message}}</div>
                        @enderror
                    </div>
                </div>
                <div class="form-group">
                    <label for="imagen">Logo de la empresa*</label>
                    <input id="imagen" class="dropify form-control @error('imagen') is-invalid @enderror" type="file" name="imagen" data-default-file="{{$emp->foto}}" data-height="250">
                    @error('imagen')
                    <div class="invalid-feedback d-block" role="alert">{{$message}}</div>
                        @enderror
                </div>
    
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Guardar</button>
                    <a href="{{ route('perfil')}}" class="btn btn-secondary">Cancelar</a>
                </div>
                
               </form>


            </div>  
           @endforeach  
@endsection

@section('script')
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
    

<script src="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/js/dropify.min.js" integrity="sha512-8QFTrG0oeOiyWo/VM9Y8kgxdlCryqhIxVeRpWSezdRRAvarxVtwLnGroJgnVW9/XBRduxO/z1GblzPrMQoeuew==" crossorigin="anonymous"></script>
<script>
    $('.dropify').dropify({
        messages: {
            'default': 'Arrastre y suelte un archivo aquí o haga clic en',
            'replace': 'Arrastra y suelta o haz clic para reemplazar',
            'remove': 'Reemplazar',
            'error': 'Necesita subir algo'
        },
        error: {
            'fileSize': 'The file size is too big (1M max).'
        }
    });
</script>
  @endsection