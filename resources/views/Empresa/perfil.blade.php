@extends('layouts.nav')

@section('content')
<a href="https://icons8.com/icon/DEY2Ab0FFfNI/ubicación-en-todo-el-mundo"></a>
<style>
    .fondo {
        background-color: white;
    }

    .py-4 {
        background-color: white;
    }

    .img-cv {
        width: 100%;
    }

    .text {
        text-align: center;
        margin-top: 3%;
    }

    .fb-profile img.fb-image-lg {
        z-index: 0;
        width: 100%;
        margin-bottom: 10px;
    }

    .fb-image-profile {
        margin: 10px 20px 80px;
        z-index: 9;
        width: 20%;
    }
    .account-settings .user-profile {
        margin: 0 0 1rem 0;
        padding-bottom: 1rem;
        text-align: center;
    }

    .account-settings .user-profile .user-avatar {
        margin: 0 0 1rem 0;
    }

    .account-settings .user-profile .user-avatar img {
        width: 90px;
        height: 90px;
        -webkit-border-radius: 100px;
        -moz-border-radius: 100px;
        border-radius: 100px;
    }

    .account-settings .user-profile h5.user-name {
        margin: 0 0 0.5rem 0;
    }

    .account-settings .user-profile h6.user-email {
        margin: 0;
        font-size: 0.8rem;
        font-weight: 400;
        color: #9fa8b9;
    }

    .account-settings .about {
        margin: 2rem 0 0 0;
        text-align: center;
    }

    .account-settings .about h5 {
        margin: 0 0 15px 0;
        color: #007ae1;
    }

    .account-settings .about p {
        font-size: 0.825rem;
    }

    .form-control {
        border: 1px solid #cfd1d8;
        -webkit-border-radius: 2px;
        -moz-border-radius: 2px;
        border-radius: 2px;
        font-size: .825rem;
        background: #ffffff;
        color: #2e323c;
    }
    .form-group{
        margin-top: 15px;
    }

    .card {
        background: #ffffff;
        -webkit-border-radius: 5px;
        -moz-border-radius: 5px;
        border-radius: 5px;
        border: 0;
        margin-bottom: 1rem;
    }
    .btn{
    color: #2255FF;
    border-color: #2255FF;
    border: solid 1px;
    background-color: white;
    border-radius: 30px;
    font-weight: bold; 
    
}
.btn:hover{
    background-color: #2255FF;
    color: white;
}
</style>
<br><br><br>
@foreach($empresa as $emp)
<h1>{{$emp->name}}</h1>
@if($emp->RFC==NULL || $emp->pais==NULL || $emp->descripcion==NULL)

<div class="container fondo">
    <div class="row ">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <img class="img-cv" src="https://i.ibb.co/RgTbgzg/dibujo-del-vector-curriculum-vitae-reclutamiento-gr-fico-documento-con-datos-personales-papel-inform.jpg" alt="dibujo-del-vector-curriculum-vitae-reclutamiento-gr-fico-documento-con-datos-personales-papel-inform" border="0">
        </div>
        <div class="col-md-4"></div>
    </div>
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10">
            <h2 class="text">Bienvenido, para poder continuar con tu registro, necesitamos que acompletes tus datos.</h2>
            <h4 class="text">¡Solo te demorará unos minutos!</h4>
            <p class="text">Nota: Si no se acompletan los datos, no podrás postularte en los empleos que tenemos para ti</p>
        </div>
        <div class="col-md-1"></div>
    </div>
    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4 text "><a href="{{url('/registro')}}">Llenar registro</a></div>
        <div class="col-md-4"></div>
    </div>
</div>
</p>


@else
<div class="container">
    <div class="row gutters">
        <div class="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12">
            <div class="card h-100">
                <div class="card-body">
                    <div class="account-settings">
                        <div class="user-profile">
                            <div class="user-avatar">
                                <img src="{{$emp->foto}}" alt="Maxwell Admin">
                            </div>
                            <h4 class="user-name">{{$emp->user->name}}</h5>
                                <h5 class="user-email">{{$emp->user->email}}</h6>
                        </div>
                        <div class="about">
                            <h5>{{$emp->cargo}}</h5>
                            <p class="text-uppercase">RFC: {{$emp->RFC}}</p>

                            <div class="row gutters">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="text-right">
                            <a href="{{ route('perfil.edit')}}" class="btn d-block"><i class="far fa-edit"></i> Editar</a>
                            </div>
                        </div>
                    </div>

                            
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-9 col-lg-9 col-md-12 col-sm-12 col-12">
            <div class="card h-100">
                <div class="card-body">
                    <div class="row gutters">
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="form-group">
                                <h4 class="mb-2 text-primary">¿Quienes Somos?</h4>
                                <h5>{{$emp->descripcion}}</h5>
                            </div>
                        </div>
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <h4 class="mb-2 text-primary">Detalles</h4>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="form-group">
                                <h4 for="fullName">Empresa</h4>
                                <h5 class="text-uppercase">{{$emp->nombre_empresa}}</h5>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="form-group">
                                <h4 for="eMail">Sitio Web</h4>
                                <h5>{{$emp->pagina_web}}</h5>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="form-group">
                                <h4 for="phone">Numero de Teléfono</h4>
                                <h5>{{$emp->telefono}}</h5>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="form-group">
                                <h4 for="website">Razón Social</h4>
                                <h5>{{$emp->razon_social}}</h5>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="form-group">
                                <h4 for="website">Giro Empresarial</h4>
                                <h5>{{$emp->giro_empresarial}}</h5>
                            </div>
                        </div>
                    </div>
                    <div class="row gutters">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <h4 class="mt-3 mb-2 text-primary">Ubicación</h4>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="form-group">
                                <h4 for="Street">País</h4>
                                <h5>{{$emp->pais}}</h5>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="form-group">
                                <h4 for="sTate">Estado</h4>
                                <h5>{{$emp->estado}}</h5>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="form-group">
                                <h4 for="ciTy">Ciudad</h4>
                                <h5>{{$emp->ciudad}}</h5>
                            </div>
                        </div>
                        
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="form-group">
                                <h4 for="zIp">Dirección</h4>
                                <h5>{{$emp->direccion}}</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endif
@endforeach
@endsection


