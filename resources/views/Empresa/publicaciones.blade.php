@extends('layouts.nav')

@section('content')
<style>
  .card {
    border-radius: 25px;
  }
</style>

<br><br><br><br>
<div class="container">
  <div class="row">
    <div class="col-sm-12">
      <h1>¡Bienvenido {{ Auth::user()->name }}!</h1>
    </div>
    <div class="col-sm-12">
      <h2>Tus publicaciones:</h2>
    </div>
  </div>
</div>
<br>

<div class="container">
  <div class="row">
    @foreach ( $empleo as $empleado )
    <div class="col-sm-4">
      <div class="card ">
        <div class="card-body">
          <ul class="list-group list-group-flush">
            <li class="list-group-item">
              <p class="card-text ">Puesto: {{$empleado->empleado->descripcion}}</p>
            </li>
            <li class="list-group-item">
              <p class="card-text">Salario: ${{$empleado->salario}}</p>
            </li>
            <div class="card-body">
              <a href="/verdetalles/{{$empleado->id}}" class="card-link">Ver postulantes</a>
              <form method="POST" action="{{ url("empresa/{$empleado->id}") }}">
                @csrf
                @method('DELETE')
                <a class="card-link">Eliminar vacante</a>
              </form>
            </div>
          </ul>
        </div>
      </div>
    </div>
    @endforeach
  </div>
</div>
@endsection