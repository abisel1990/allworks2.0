@extends('layouts.nav')

@section('content')

<style>
  .btn-search {
    color: white;
    background-color: #002CBF;
    margin-left: 90%;
    margin-bottom: 10%;
  }

  .btn-search:hover {
    color: white;
  }

  .footer {
    width: 100%;
    background-color: #f5f5f5;
    padding: 1%;
    bottom: 0;
    position: relative;
  }

  .py-4 {
    background-color: white;
  }
  .top1{
    margin-top: 120px;
}
</style>


<form method="get" action="{{ route('buscarempleo') }}">


  <form method="get" action="{{ route('buscarempleo') }}">
    <div class="row top1">
      <div class="col-md-7">
      </div>
      <div class="form-group">
        <div class="col-md-4">
        <input type="text" placeholder="Buscar empleo" name="search">
          <!-- <select class="form-control search" name="search" id='selUser' style='width: 180px;'>
            <option value='0'>Elije un empleo </option>
            <option value='1'>arquitecto </option>
            <option value='2'>biólogo </option>
            <option value='3'>químico </option>
            <option value='4'>dentista </option>
            <option value='5'>diseñador </option>
            <option value='6'>médico </option>
            <option value='7'>ingeniero </option>
            <option value='8'>diseñador gráfico </option>
            <option value='9'>periodista </option>
            <option value='10'>enfermera </option>
            <option value='11'>maestro </option>
            <option value='12'>veterinario </option>
            <option value='13'>escritor </option>
            <option value='14'>cajero </option>
<<<<<<< HEAD

          </select> -->
=======
          </select>
>>>>>>> 1c496e1c37d8da6ed80d4956ec347a107ca0c60b
        </div>
      </div>
      <div class="col-md-1">
        <button type="submit" class="btn btn-search">Buscar</button>
      </div>
    </div>
  </form>


</form>
<div class="container">
  <div class="row">
    @if (! empty($empleos))

    @foreach($empleos as $empleo)
    <div class="col-sm-4">
      <div class="card">
        <div class="card-body">
          <img class="card-img-top" src="{{$empleo->empresa->foto}}" alt="abisel" style="width: 100%;">
          <div class="card-body">
            <h5 class="card-title">{{$empleo->empleado->descripcion}}</h5>
          </div>
          <ul class="list-group list-group-flush">
            <li class="list-group-item">Empresa: <a>{{$empleo->empresa->nombre_empresa}}</a></li>
            <li class="list-group-item">Salario: <a>{{$empleo->salario}}</a></li>
            <li class="list-group-item">Dirección: <a>{{$empleo->empresa->direccion}} , {{$empleo->empresa->estado}} , {{$empleo->empresa->pais}} </a></li>
          </ul>
          <div class="card-body">
            <a href="#" class="card-link" data-toggle="modal" data-target="#empleo-{{$empleo->id}}">Ver detalles</a>
          </div>
        </div>
      </div>
    </div>
    @endforeach

    @else
    <p>No hay empleos registrados.</p>
    @endif





    @if (! empty($empleos))

    @foreach($empleos as $empleo)
    <div id="empleo-{{$empleo->id}}" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">{{$empleo->empleado->descripcion}}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <h5>Descripción: {{$empleo->descripcion}}</h5>
            <h5>Salario: ${{$empleo->salario}}</h5>
            <h5>Descripcion: {{$empleo->descripcion}}</h5>
            <h5>Empresa: {{$empleo->empresa->nombre_empresa}}</h5>
            <h5>País: {{$empleo->empresa->pais}}</h5>
            <h5>Estado: {{$empleo->empresa->estado}}</h5>
            <h5>Ciudad: {{$empleo->empresa->ciudad}}</h5>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#postularme-{{$empleo->id}}">Postularme</button>
          </div>
        </div>
      </div>
    </div>
    @endforeach


    @else
    <p>No hay empleos registrados.</p>
    @endif



    @if (! empty($empleos))
    @foreach($empleos as $empleo)


    <div id="postularme-{{$empleo->id}}" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">

          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">{{$empleo->empleado->descripcion}}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <h1>Hola: {{auth()->user()->name}}</h1>
            <h1>¿Estás seguro que quieres postularte a este empleo? </h1>
            <p>Nota: Los datos registrados en tu curriculum se enviaran a la empresa para valorarte</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            @foreach($postulante as $post)

            @if($post->fecha_nacimiento == null || $post->telefono == null || $post->nacionalidad == null )
            <a href="/empleos" id="postularme">Postularme</a>
            @else
            <a href="/postularme/{{$empleo->id}}/{{$empleo->empresa->id}}">Postularme</a>
            @endif
            @endforeach
            <!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#postularme-{{$empleo->id}}">Postularme</button> -->

          </div>
        </div>
      </div>
    </div>
</div>
@endforeach
@endif
@endsection
@section('script')

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js"></script>
<script type="text/javascript">
  $('a#postularme').click(function(e) {
    e.preventDefault();
    alert('Necesitas llenar los datos de tu perfil');
  });

  $(document).ready(function() {
        $("body").on("click", ".mi-elemento", function() {
          var data_numid = $(this).attr('data-numid'),
            data_nombre = $(this).attr('data-nombre'),
            data_estado = $(this).attr('data-estado'),
            data_encuesta = $(this).attr('data-encuesta');

          $("#modalClonarEncuesta").modal("show");
          $("#modalClonarEncuesta .modal-title").html("Mi título asombroso");
          $("#hiddenIdClonarEncuesta").val(data_numid);

          alert("El valor del input es ---> " + document.getElementById("hiddenIdClonarEncuesta").value);
        });



        $('#tags').select2({
          // Activamos la opcion "Tags" del plugin
          tokenSeparators: [','],
          language: "es",

          ajax: {
            url: '{{url("tags") }}',
            dataType: 'json',
            // dataType: 'json',
            // url: '{{ url("tags") }}',
            delay: 250,
            data: function(params) {
              return {
                term: params.term
              }
            },
            processResults: function(data, page) {
              return {
                results: data
              };
            },
          },
          //     escapeMarkup: function (markup) { return markup; },
          // minimumInputLength: 1,
          // escapeMarkup: function (markup) { return markup; },
          // minimumInputLength: 1,
          // templateResult: formatRepoClientes,
          // templateSelection: formatRepoSelectionClientes

        });


        $(document).ready(function() {
          // inicializamos el plugin


        });
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $("#selUser").select2({
          ajax: {
            url: "{{route('tags')}}",
            type: "post",
            dataType: 'json',
            delay: 250,
            data: function(params) {
              return {
                _token: CSRF_TOKEN,
                search: params.term // search term
              };
            },
            processResults: function(response) {
              return {
                results: response
              };
            },
            cache: true
          },
          minimumInputLength: 2,

        });


        $('#tags').select2({
          // Activamos la opcion "Tags" del plugin
          tokenSeparators: [','],
          language: "es",

          ajax: {
            url: '{{url("tags") }}',
            dataType: 'json',
            // dataType: 'json',
            // url: '{{ url("tags") }}',
            delay: 250,
            data: function(params) {
              return {
                term: params.term
              }
            },
            processResults: function(data, page) {
              return {
                results: data
              };
            },
          },
          // escapeMarkup: function (markup) { return markup; },
          // minimumInputLength: 1,
          // escapeMarkup: function (markup) { return markup; },
          // minimumInputLength: 1,
          // templateResult: formatRepoClientes,
          // templateSelection: formatRepoSelectionClientes

        });


        $(document).ready(function() {
          // inicializamos el plugin

        });
</script>



@endsection