@extends('layouts.nav')

@section('content')
<style>
  .card {
    border-radius: 25px;
  }
  .top1{
    margin-top: 120px;
  }
</style>


<div class="container">
  <div class="row top1">
    <div class="col-md-12">
      <h2>Mis postulaciones.</h2>
    </div>
    <br>
    <div class="col-md-12">
      <p>En esta sección se muestran todos los puestos de trabajo a los que has enviado solicitud:</p>
    </div>
    <br>
    <br>
  </div>
  <div class="row">
    @foreach($mis_postulaciones as $postulaciones)

    <div class="col-md-5">
      <div class="card ">
        <div class="card-body">
          <ul class="list-group list-group-flush">
            <li class="list-group-item">
              <p class="card-text ">Nombre Empresa: {{$postulaciones->empresa->nombre_empresa}} </p>
            </li>
            <li class="list-group-item">
              <p class="card-text">Descripcion: {{$postulaciones->empleado->descripcion}}</p>
            </li>
            <div class="card-body">
              <a href="" class="card-link">Ver detalles</a>
              <form method="POST" action="">
              </form>
            </div>
          </ul>
        </div>
      </div>
    </div>
    @endforeach
  </div>
</div>






@endsection