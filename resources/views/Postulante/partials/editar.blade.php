@extends('layouts.nav')

@section('content')
<style>
    .top1 {
        margin-top: 120px;
    }
</style>
@foreach($postulante as $emp)
<div class="container">
    <div class="row top1">
        <div class="col-md-12 mx-auto bg-white p-3">
            <form action="{{ route('perfil.actualizar') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="form-group col-md-4">
                        <label for="fecha_nacimiento">Fecha de nacimiento:</label>
                        <input id="fecha_nacimiento" class="form-control @error('rezon_social') is-invalid @enderror" type="text" name="fecha_nacimiento" value="{{$emp->fecha_nacimiento}}" />
                        @error('fecha_nacimiento')
                        <div class="invalid-feedback d-block" role="alert">{{$message}}</div>
                        @enderror
                    </div>

                    <div class="form-group col-md-4">
                        <label for="genero">Genero:</label>
                        <select id="genero" class="form-control @error('genero') is-invalid @enderror" type="text" name="genero" value="{{$emp->genero}}">
                            <option>Masculino</option>
                            <option>Femenino</option>
                            <option>Indefinido</option>
                        </select>
                        @error('genero')
                        <div class="invalid-feedback d-block" role="alert">{{$message}}</div>
                        @enderror
                    </div>
                    <div class="form-group col-md-4">
                        <label for="estado_civil">Estado civil:</label>
                        <select id="estado_civil" class="form-control @error('estado_civil') is-invalid @enderror" type="text" name="estado_civil" value="{{$emp->estado_civil}}">
                            <option>Soltero</option>
                            <option>Casado</option>
                            <option>Divorsiado</option>
                            <option>Separación en proceso</option>
                            <option>Viudo</option>
                            <option>Concubinato</option>
                        </select>
                        @error('estado_civil')
                        <div class="invalid-feedback d-block" role="alert">{{$message}}</div>
                        @enderror
                    </div>
                    <div class="form-group col-md-4">
                        <label for="pais">Pais:</label>
                        <select id="pais" class="form-control @error('pais') is-invalid @enderror" type="text" name="pais" value="{{$emp->pais}}">
                            <option>México</option>
                        </select>
                        @error('pais')
                        <div class="invalid-feedback d-block" role="alert">{{$message}}</div>
                        @enderror
                    </div>
                    <div class="form-group col-md-4">
                        <label for="estado">Estado</label>
                        <select id="estado" class="form-control @error('estado') is-invalid @enderror" type="text" name="estado" value="{{$emp->estado}}">
                            <option>{{$emp->estado}}</option>
                            <option>Campeche</option>
                            <option>Quintana Roo</option>
                            <option>Yucantán</option>
                        </select>
                        @error('estado')
                        <div class="invalid-feedback d-block" role="alert">{{$message}}</div>
                        @enderror
                    </div>
                    <div class="form-group col-md-4">
                        <label for="ciudad">Ciudad:</label>
                        <input id="ciudad" class="form-control @error('ciudad') is-invalid @enderror" type="text" name="ciudad" value="{{$emp->ciudad}}" />
                        @error('ciudad')
                        <div class="invalid-feedback d-block" role="alert">{{$message}}</div>
                        @enderror
                    </div>
                </div>
                <div class="form-group">
                    <label for="cargo">Cargo::</label>
                    <select id="cargo" class="form-control @error('descripcion') is-invalid @enderror" name="cargo" value="{{ $emp->cargo}}">
                        <option>Jefe</option>

                        }
                    </select>
                    @error('descripcion')
                    <div class="invalid-feedback d-block" role="alert">{{$message}}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="descripcion">Descripción del Cargo::</label>
                    <textarea id="descripcion" class="form-control @error('descripcion') is-invalid @enderror" name="descripcion">{{$emp->descripcion_cargo}}</textarea>
                    @error('descripcion')
                    <div class="invalid-feedback d-block" role="alert">{{$message}}</div>
                    @enderror
                </div>
                <div class="row">
                    <div class="form-group col-md-6">
                        <label for="telefono">Telefono:</label>
                        <input id="telefono" class="form-control @error('telefono') is-invalid @enderror" type="number" step="0.01" name="telefono" value="{{$emp->telefono}}" />
                        @error('telefono')
                        <div class="invalid-feedback d-block" role="alert">{{$message}}</div>
                        @enderror
                    </div>
                </div>
                <div class="form-group">
                    <label for="imagen">Foto</label>
                    <input id="imagen" class="dropify form-control @error('imagen') is-invalid @enderror" type="file" name="imagen" data-default-file="{{$emp->foto}}" data-height="250">
                    @error('imagen')
                    <div class="invalid-feedback d-block" role="alert">{{$message}}</div>
                    @enderror
                </div>
                <br><br><br><br><br><br>
                <div class="row">
                    <div class="col-md-9"></div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <a href="" class="btn btn-secondary">Cancelar</a>
                            <button type="submit" class="btn btn-primary">Guardar</button>
                        </div>
                    </div>
                </div>

            </form>


        </div>
    </div>
</div>
@endforeach
@endsection

@section('script')
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/js/dropify.min.js" integrity="sha512-8QFTrG0oeOiyWo/VM9Y8kgxdlCryqhIxVeRpWSezdRRAvarxVtwLnGroJgnVW9/XBRduxO/z1GblzPrMQoeuew==" crossorigin="anonymous"></script>
<script>
    $('.dropify').dropify({
        messages: {
            'default': 'Arrastre y suelte un archivo aquí o haga clic en',
            'replace': 'Arrastra y suelta o haz clic para reemplazar',
            'remove': 'Reemplazar',
            'error': 'Necesita subir algo'
        },
        error: {
            'fileSize': 'The file size is too big (1M max).'
        }
    });
</script>
@endsection