@extends('layouts.nav')

@section('content')
<style>
    .top1 {
        margin-top: 120px;
    }
</style>
<div class="container">
    <div class="row top1">
        <div class="col-md-12 mx-auto bg-white p-3">
            <form action="{{ route('storePostulante') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="form-group col-md-4">
                        <label for="fecha_nacimiento">Fecha de Nacimiento:</label>
                        <input type="date" id="fecha_nacimiento" class="form-control @error('rezon_social') is-invalid @enderror" type="text" name="fecha_nacimiento" value="{{ old('fecha_nacimiento')}}" />
                        @error('fecha_nacimiento')
                        <div class="invalid-feedback d-block" role="alert">{{$message}}</div>
                        @enderror
                    </div>
                    <div class="form-group col-md-4">
                        <label for="genero">Genero:</label>
                        <select id="genero" class="form-control @error('genero') is-invalid @enderror" type="text" name="genero" value="{{ old('genero')}}">
                            <option>Masculino</option>
                            <option>Femenino</option>
                            <option>Indefinido</option>
                        </select>
                        @error('genero')
                        <div class="invalid-feedback d-block" role="alert">{{$message}}</div>
                        @enderror
                    </div>
                    <div class="form-group col-md-4">
                        <label for="estado_civil">Estado civil:</label>
                        <select id="estado_civil" class="form-control @error('estado_civil') is-invalid @enderror" type="text" name="estado_civil" value="{{old ('estado_civil')}}">
                            <option>Soltero</option>
                            <option>Casado</option>
                            <option>Divorsiado</option>
                            <option>Separación en proceso</option>
                            <option>Viudo</option>
                            <option>Concubinato</option>
                        </select>
                        @error('estado_civil')
                        <div class="invalid-feedback d-block" role="alert">{{$message}}</div>
                        @enderror
                    </div>
                    <div class="form-group col-md-4">
                        <label for="pais">Pais:</label>
                        <select id="pais" class="form-control @error('pais') is-invalid @enderror" type="text" name="pais" value="{{ old('pais')}}">
                            <option>México</option>
                        </select>
                        @error('pais')
                        <div class="invalid-feedback d-block" role="alert">{{$message}}</div>
                        @enderror
                    </div>
                    <div class="form-group col-md-4">
                        <label for="estado">Estado:</label>
                        <select id="estado" class="form-control @error('estado') is-invalid @enderror" type="text" name="estado" value="{{ old('estado')}}">
                            <option>Campeche</option>
                            <option>Quintana Roo</option>
                            <option>Yucantán</option>
                        </select>
                        @error('estado')
                        <div class="invalid-feedback d-block" role="alert">{{$message}}</div>
                        @enderror
                    </div>
                    <div class="form-group col-md-4">
                        <label for="ciudad">Ciudad:</label>
                        <input id="ciudad" class="form-control @error('ciudad') is-invalid @enderror" type="text" name="ciudad" value="{{ old('ciudad')}}" />
                        @error('ciudad')
                        <div class="invalid-feedback d-block" role="alert">{{$message}}</div>
                        @enderror
                    </div>
                    <div class="form-group col-md-4">
                        <label for="nacionalidad">Nacionalidad:</label>
                        <input id="nacionalidad" class="form-control @error('nacionalidad') is-invalid @enderror" type="text" name="nacionalidad" value="{{ old('nacionalidad')}}" />
                        @error('nacionalidad')
                        <div class="invalid-feedback d-block" role="alert">{{$message}}</div>
                        @enderror
                    </div>

                    <div class="form-group col-md-4">
                        <label for="discapacidad">Cuenta con alguna discapacidad:</label>
                        <select id="discapacidad" class="form-control @error('discapacidad') is-invalid @enderror" name="discapacidad">{{old('discapacidad')}}
                            <option value="0">No</option>
                            <option value="1">Si</option>
                        </select>
                        @error('discapacidad')
                        <div class="invalid-feedback d-block" role="alert">{{$message}}</div>
                        @enderror
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-4">
                        <label for="cargo">Cargo:</label>
                        <input id="cargo" class="form-control @error('cargo') is-invalid @enderror" type="text" name="cargo" value="{{ old('cargo')}}" />
                        @error('cargo')
                        <div class="invalid-feedback d-block" role="alert">{{$message}}</div>
                        @enderror
                    </div>
                    <div class="form-group col-md-6">
                        <label for="descripcion_cargo">Descripcion del Cargo:</label>
                        <input id="descripcion_cargo" class="form-control @error('descripcion_cargo') is-invalid @enderror" type="text" name="descripcion_cargo" value="{{ old('descripcion_cargo')}}" />
                        @error('descripcion_cargo')
                        <div class="invalid-feedback d-block" role="alert">{{$message}}</div>
                        @enderror
                    </div>
                    <div class="form-group col-md-6">
                        <label for="telefono">Telefono:</label>
                        <input id="telefono" class="form-control @error('telefono') is-invalid @enderror" type="number" step="0.01" name="telefono" value="{{ old('telefono')}}" />
                        @error('telefono')
                        <div class="invalid-feedback d-block" role="alert">{{$message}}</div>
                        @enderror
                    </div>
                </div>
                <div class="form-group">
                    <label for="imagen">Foto</label>
                    <input id="imagen" class="dropify form-control @error('imagen') is-invalid @enderror" type="file" name="imagen">
                    @error('imagen')
                    <div class="invalid-feedback d-block" role="alert">{{$message}}</div>
                    @enderror
                </div>

                <br><br><br><br><br><br>
                <div class="row">
                    <div class="col-md-9"></div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <a href="" class="btn btn-secondary">Cancelar</a>
                            <button type="submit" class="btn btn-primary">Guardar</button>
                        </div>
                    </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('script')
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/js/dropify.min.js" integrity="sha512-8QFTrG0oeOiyWo/VM9Y8kgxdlCryqhIxVeRpWSezdRRAvarxVtwLnGroJgnVW9/XBRduxO/z1GblzPrMQoeuew==" crossorigin="anonymous"></script>
<script>
    $('.dropify').dropify({
        messages: {
            'default': 'Arrastre y suelte un archivo aquí o haga clic en',
            'replace': 'Arrastra y suelta o haz clic para reemplazar',
            'remove': 'Reemplazar',
            'error': 'Necesita subir algo'
        },
        error: {
            'fileSize': 'The file size is too big (1M max).'
        }
    });
</script>
@endsection