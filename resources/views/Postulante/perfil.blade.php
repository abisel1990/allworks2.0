@extends('layouts.nav')

@section('content')
<style>
    .fondo {
        background-color: white;
    }

    .py-4 {
        background-color: white;
    }

    .img-cv {
        width: 100%;
    }

    .text {
        text-align: center;
        margin-top: 3%;
    }

    .fb-profile img.fb-image-lg {
        z-index: 0;
        width: 100%;
        margin-bottom: 10px;
    }

    .fb-image-profile {
        margin: 10px 20px 80px;
        z-index: 9;
        width: 20%;
    }

    /***
Bootstrap Line Tabs by @keenthemes
A component of Metronic Theme - #1 Selling Bootstrap 3 Admin Theme in Themeforest: http://j.mp/metronictheme
Licensed under MIT
***/

    /* Tabs panel */

    .account-settings .user-profile {
        margin: 0 0 1rem 0;
        padding-bottom: 1rem;
        text-align: center;
    }

    .account-settings .user-profile .user-avatar {
        margin: 0 0 1rem 0;
    }

    .account-settings .user-profile .user-avatar img {
        width: 90px;
        height: 90px;
        -webkit-border-radius: 100px;
        -moz-border-radius: 100px;
        border-radius: 100px;
    }

    .account-settings .user-profile h5.user-name {
        margin: 0 0 0.5rem 0;
    }

    .account-settings .user-profile h6.user-email {
        margin: 0;
        font-size: 0.8rem;
        font-weight: 400;
        color: #9fa8b9;
    }

    .account-settings .about {
        margin: 2rem 0 0 0;
        text-align: center;
    }

    .account-settings .about h5 {
        margin: 0 0 15px 0;
        color: #007ae1;
    }

    .account-settings .about p {
        font-size: 0.825rem;
    }

    .form-control {
        border: 1px solid #cfd1d8;
        -webkit-border-radius: 2px;
        -moz-border-radius: 2px;
        border-radius: 2px;
        font-size: .825rem;
        background: #ffffff;
        color: #2e323c;
    }

    .form-group {
        margin-top: 15px;
    }

    .card {
        background: #ffffff;
        -webkit-border-radius: 5px;
        -moz-border-radius: 5px;
        border-radius: 5px;
        border: 0;
        margin-bottom: 1rem;
    }

    .d-block {
        background-color: #2255FF;
        color: #ffffff;
        border-radius: 30px;
        border: none;
        display: inline-block;
        font-weight: 400;
        text-align: center;
        white-space: nowrap;
        vertical-align: middle;
        user-select: none;
        border: 1px solid transparent;
        padding: .375rem .75rem;
        font-size: 1rem;
        line-height: 1.5;
        border-radius: 25px;
    }

    .d-block:hover {
        background-color: #ffffff;
        color: #2255FF;
        border: solid 1px;
    }
    .top1{
    margin-top: 100px;
}
</style>


@foreach($postulante as $emp)

<h1>{{$emp->name}}</h1>

@if($emp->genero==NULL || $emp->estado_civil==NULL || $emp->cargo==NULL)

<div class="container fondo top1">
    <div class="row ">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <img class="img-cv" src="https://i.ibb.co/RgTbgzg/dibujo-del-vector-curriculum-vitae-reclutamiento-gr-fico-documento-con-datos-personales-papel-inform.jpg" alt="dibujo-del-vector-curriculum-vitae-reclutamiento-gr-fico-documento-con-datos-personales-papel-inform" border="0">
        </div>
        <div class="col-md-4"></div>
    </div>

    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10">
            <h2 class="text">Bienvenido, para poder continuar con tu registro, necesitamos que acompletes tus datos.</h2>
            <h5 class="text">¡Solo te demorará unos minutos!</h5>
            <p class="text">Nota: Si no se acompletan los datos, no podrás postularte en los empleos que tenemos para ti</p>
        </div>
        <div class="col-md-1"></div>
    </div>

    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4"><a href="{{url('/registroPostulante')}}" class="btn d-block"><i class="far fa-edit"></i> Llenar registro</a></div>
        <div class="col-md-4"></div>
    </div>
</div>
<br>
<br>
<br>
<br>

@else
<div class="container">
    <div class="row gutters top1">
        <div class="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12">
            <div class="card h-100">
                <div class="card-body">
                    <div class="account-settings">
                        <div class="user-profile">
                            <div class="user-avatar">
                                <img src="{{$emp->foto}}" alt="Maxwell Admin">
                            </div>
                            <h5 class="user-name">{{$emp->user->name}}</h5>
                            <h5 class="user-email">{{$emp->user->email}}</h6>
                        </div>
                        <div class="about">
                            <h5>{{$emp->cargo}}</h5>
                            <p>{{$emp->descripcion_cargo}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-9 col-lg-9 col-md-12 col-sm-12 col-12">
            <div class="card h-100">
                <div class="card-body">
                    <div class="row gutters">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <h5 class="mb-2 text-primary">Detalles Personales</h5>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="form-group">
                                <h5 for="fullName">Género:</h5>
                                <h5>{{$emp->genero}}</h5>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="form-group">
                                <h5 for="eMail">Estado Civil:</h5>
                                <h5>{{$emp->estado_civil}}</h5>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="form-group">
                                <h5 for="phone">Télefono:</h5>
                                <h5>{{$emp->telefono}}</h5>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="form-group">
                                <h5 for="website">Discapacidad:</h5>
                                @if($emp->discapacidad==0)
                                <h5>Ninguna</h5>
                                @else
                                <h5>Si, cuento con alguna discapacidad</h5>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row gutters">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <h5 class="mt-3 mb-2 text-primary">Localización:</h5>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="form-group">
                                <h5 for="Street">Pais:</h5>
                                <h5>{{$emp->pais}}</h5>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="form-group">
                                <h5 for="ciTy">Estado:</h5>
                                <h5>{{$emp->estado}}</h5>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="form-group">
                                <h5 for="sTate">Ciudad:</h5>
                                <h5>{{$emp->ciudad}}</h5>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="form-group">
                                <h5 for="zIp">Nacionalidad:</h5>
                                <h5>{{$emp->nacionalidad}}</h5>
                            </div>
                        </div>
                    </div>
                    <br>
                    <br>
                    <br>
                    <div class="row gutters">
                        <div class="col-md-2"></div>
                        <div class="col-md-4"><a href="{{ route('perfil.editPostulante')}}" class="btn d-block"><i class="far fa-edit"></i> Editar</a></div>
                        <div class="col-md-6"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<br>
<br>
<br>
<br>
<br>
<!-- <a href="{{ route('perfil.editPostulante')}}" class="btn btn-warning  d-block"><i class="far fa-edit"></i> Editar</a>
<img src="{{$emp->foto}}" alt="abisel" style="width:300px;height:300px" > -->
@endif


@endforeach
@endsection