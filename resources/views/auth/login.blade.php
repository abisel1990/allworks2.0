@extends('layouts.app')
@section('content')


<style>
.btn{
  color: white;
  background-color: #2255FF;
  width:50%;

  }
.btn:hover{
  background-color: #002CBF;
  color: white;
  }
.img{
    width:15%;
    margin-left:42%;
}
.register1{
    margin-top: 100px;
}
</style>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8 register1">
            <div>       
           <img class="img" src="https://i.ibb.co/1G8HKvx/logo.png" alt="logo" border="0"></a>      
            <div class="card-body text-center"><h4>{{ __('¡Bienvenido de nuevo!') }}<h4>
             
                </div>
        
                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email"  class="col-md-3 col-form-label text-md-right"></label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror rounded-pill" placeholder="Correo Electronico" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-3 col-form-label text-md-right"></label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror rounded-pill" placeholder="Contraseña" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row text-center">
                            <div class="col-md-6 offset-md-3">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Mantener Sesión') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row text-center">
                            <div class="col-md-8 offset-md-2 ">
                                <button type="submit" class="btn rounded-pill">
                                    {{ __('Iniciar Sesión') }}
                                </button>
                            </div>
                        </div>
                        <div class="form-group row text-center">
                            <div class="col-md-8 offset-md-2">
                                @if (Route::has('password.request'))
                                    <a class="btn btn-link " href="{{ route('password.request') }}">
                                        {{ __('¿Olvidaste tu contraseña?') }}
                                    </a>
                                @endif
                                
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<br>
<br>
<br>
<br>
<br>
<br>


@endsection
