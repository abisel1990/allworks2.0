@extends('layouts.app')
@section('content')

<style>
.btn{
  color: white;
  background-color: #2255FF;
  width:70%;
  border-radius: 30px;
}
.btn:hover{
  background-color: #002CBF;
  color: white;
  }
  
</style>


<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
        <div class="card-body"><h3>Soporte de Allworks</h3></div>
            <div class="card">
                <div class="card-body"><h5>{{ __('He olvidado la contraseña de mi cuenta') }}</h5><h6>Le enviaremos un enlace para restablecer su contraseña por correo electrónico</h6></div>
                
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf

                        <div class="form-group row">
                            <div class="col-md-5">
                                <input id="email" placeholder="Introduce tu correo electrónico" type="email" class="form-control @error('email') is-invalid @enderror rounded-pill" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                <br>
                                <div>
                                <button type="submit" class="btn rounded-pill">
                                    {{ __('Enviar enlace') }}
                                </button>
                            </div>
                        </div>
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
