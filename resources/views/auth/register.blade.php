@extends('layouts.app')
@section('content')

<style>
.btn{
  color: white;
  background-color: #2255FF;
  width:50%;
  border-radius: 30px;
}
.btn:hover{
  background-color: #002CBF;
  color: white;
  }
  .img{
    width:15%;
    margin-left:42%;
}
.register1{
    margin-top: 100px;
}
</style>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8 register1">
            <div>
            <img class="img" src="https://i.ibb.co/1G8HKvx/logo.png" alt="logo" border="0"></a>
                <div class="card-body text-center"><h4>{{ __('¡Bienvenido abordo!') }}</h4>
                

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                        <label for="email"  class="col-md-3 col-form-label text-md-right"></label>
                            <div class="col-md-6">
                                <input id="name" type="text" placeholder="Nombre" class="rounded-pill form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-3 col-form-label text-md-right"></label>

                            <div class="col-md-6">
                                <input id="email" type="email" placeholder="Correo Electrónico" class="rounded-pill form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-3 col-form-label text-md-right"></label>

                            <div class="col-md-6">
                                <input id="password" type="password" placeholder="Contraseña" class="rounded-pill form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-3 col-form-label text-md-right"></label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control rounded-pill" placeholder="Confirmar Contraseña" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>
                        <div class="form-group row text-center">
                            <label for="password" class="col-md-3 col-form-label text-md-right"></label>

                            <div class="col-md-6">
                                <!-- <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password"> -->
                                
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="id_rol" id="id_rol" value="2" required autocomplete="registrame como:">
                                    <label  class="form-check-label" for="inlineRadio1">Postulante</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="id_rol" id="id_rol" value="1" required autocomplete="registrame como:">
                                    <label  class="form-check-label" for="inlineRadio2">Empresa</label>
                                </div>
                                
                               <!-- <select name="id_rol" id="id_rol" class="form-control @error('id_rol') is-invalid @enderror" required autocomplete="registrame como:">
                                    <option value="2">Postulante</option>
                                    <option value="1">Empresa</option>
                                </select>
                                -->

                                @error('id_rol')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row text-center">
                            <div class="col-md-8 offset-md-2">
                                <button type="submit" class=" rounded-pill btn">
                                    {{ __('Registrarse') }}
                                </button>
                            </div>
                        </div>
                        
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<br>
<br>
<br>

@endsection

