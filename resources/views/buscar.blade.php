
@extends('layouts.nav')

@section('content')

<style>
  .btn-search {
    color: white;
    background-color: #002CBF;
    margin-left: 90%;
    margin-bottom: 10%;
  }

  .btn-search:hover {
    color: white;
  }

  .footer {
    width: 100%;
    background-color: #f5f5f5;
    padding: 1%;
    bottom: 0;
    position: relative;
  }

  .py-4 {
    background-color: white;
  }
  .top1{
    margin-top: 120px;
}
</style>





<div class="container">
  <div class="row">
    @if (! empty($clientes))

    @foreach($clientes as $empleo)
    <div class="col-sm-4">
      <div class="card">
        <div class="card-body">
          <img class="card-img-top" src="{{$empleo->foto}}" alt="abisel" style="width: 100%;">
          <div class="card-body">
            <h5 class="card-title"></h5>
          </div>
          <ul class="list-group list-group-flush">
            <li class="list-group-item">Empresa: <a>{{$empleo->nombre_empresa}}</a></li>
            <li class="list-group-item">Salario: <a>{{$empleo->salario}}</a></li>
            <li class="list-group-item">Dirección: <a>{{$empleo->direccion}} , {{$empleo->estado}} , {{$empleo->pais}} </a></li>
          </ul>
          <div class="card-body">
            <a href="#" class="card-link" data-toggle="modal" data-target="#empleo-{{$empleo->id}}">Ver detalles</a>
          </div>
        </div>
      </div>
    </div>
    @endforeach

    @else
    <p>No hay empleos registrados.</p>
    @endif





    @if (! empty($clientes))

    @foreach($clientes as $empleo)
    <div id="empleo-{{$empleo->id}}" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <h5>Descripción:</h5>
            <h5>Salario: ${{$empleo->salario}}</h5>
            <h5>Descripcion: </h5>
            <h5>Empresa: {{$empleo->nombre_empresa}}</h5>
            <h5>País: {{$empleo->pais}}</h5>
            <h5>Estado: {{$empleo->estado}}</h5>
            <h5>Ciudad: {{$empleo->ciudad}}</h5>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#postularme-{{$empleo->id}}">Postularme</button>
          </div>
        </div>
      </div>
    </div>
    @endforeach


    @else
    <p>No hay empleos registrados.</p>
    @endif



    @if (! empty($clientes))
    @foreach($clientes as $empleo)


    <div id="postularme-{{$empleo->id}}" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">

          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <h1>Hola: {{auth()->user()->name}}</h1>
            <h1>¿Estás seguro que quieres postularte a este empleo? </h1>
            <p>Nota: Los datos registrados en tu curriculum se enviaran a la empresa para valorarte</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            @foreach($postulante as $post)

            @if($post->fecha_nacimiento == null || $post->telefono == null || $post->nacionalidad == null )
            <a href="/empleos" id="postularme">Postularme</a>
            @else
            <a href="/postularme/{{$empleo->id}}/{{$empleo->empresa->id}}">Postularme</a>
            @endif
            @endforeach
            <!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#postularme-{{$empleo->id}}">Postularme</button> -->

          </div>
        </div>
      </div>
    </div>
</div>
@endforeach
@endif
@endsection
@section('script')

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js"></script>



@endsection