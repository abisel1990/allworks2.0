<footer id="footer">

<div class="footer-top">
  <div class="container">
    <div class="row">

      <div class="col-lg-3 col-md-6 footer-contact">
        <h3></h3>
        <p>Allwork's
          Calle 30 <br>
          Cancún , Q.roo 55117<br>
          México <br><br>
          <strong>Telefono:</strong> 9982585720<br>
          <strong>Email:</strong> allworks@example.com<br>
        </p>
      </div>

      <div class="col-lg-2 col-md-6 footer-links">
        <h4>Links por defecto:</h4>
        <ul>
          <li><i class="bx bx-chevron-right"></i> <a href="/">Inicio</a></li>
          <li><i class="bx bx-chevron-right"></i> <a href="{{ route('nosotros') }}">¿Quienes Somos?</a></li>
          <li><i class="bx bx-chevron-right"></i> <a href="#">Servicios</a></li>
          <li><i class="bx bx-chevron-right"></i> <a href="#">Terminos y Condiciones</a></li>
          <li><i class="bx bx-chevron-right"></i> <a href="#">Privacidad y Politica</a></li>
        </ul>
      </div>

      <div class="col-lg-3 col-md-6 footer-links">
        <h4>Redes Sociales:</h4>
        <ul>
          <li><i class="bx bx-chevron-right"></i> <a href="#">Facebook</a></li>
          <li><i class="bx bx-chevron-right"></i> <a href="#">Twitter</a></li>
          <li><i class="bx bx-chevron-right"></i> <a href="#">Instagram</a></li>
          <li><i class="bx bx-chevron-right"></i> <a href="#">Skype</a></li>
        </ul>
      </div>

      <div class="col-lg-4 col-md-6 footer-newsletter">
        <h4>Seguiremos trabajando para ti:</h4>
        <p>Recuerda mandarnos tus sugerencial al siguiente correo: allworks@example.com</p>
      </div>

    </div>
  </div>
</div>

<div class="container d-md-flex py-4">

  <div class="mr-md-auto text-center text-md-left">
    <div class="copyright">
      &copy; Copyright <strong><span>Allworks</span></strong>. Todos los derechos reservados
    </div>
    <div class="credits">
      <!-- All the links in the footer should remain intact. -->
      <!-- You can delete the links only if you purchased the pro version. -->
      <!-- Licensing information: https://bootstrapmade.com/license/ -->
      <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/onepage-multipurpose-bootstrap-template/ -->
      Designed by <a href="https://www.facebook.com/chistian.tunpoot/">Tun Poot Christian</a>
    </div>
  </div>
  <div class="social-links text-center text-md-right pt-3 pt-md-0">
    <a href="#" class="twitter"><i class="fab fa-facebook-square"></i></a>
    <a href="#" class="facebook"><i class="fab fa-twitter"></i></a>
    <a href="#" class="instagram"><i class="fab fa-instagram"></i></a>
    <a href="#" class="google-plus"><i class="fab fa-skype"></i></a>
  </div>
</div>
</footer><!-- End Footer -->