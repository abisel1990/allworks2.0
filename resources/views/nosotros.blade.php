@extends('layouts.app')
@section('content')


<style>
.foto {
  width: 70%;
  border-radius:50%;
}
</style>

<br><br><br><br>
<div class="container">
  <div class="row">
    <div class="col order-last">
    <img src="https://i.ibb.co/TqFLDqR/Mapa-CT19.png" alt="Mapa-CT19" border="0"></a>
    </div>
    <div class="col">
      <h2>Lider en América Latina</h2>
      <p>Allworks es la web de empleo líder en Latinoamérica con presencia en 19 países de los cuales es líder en 10.</p>
      <p>Es la bolsa de trabajo con más visitas en Colombia, Perú, Argentina, Uruguay, Guatemala, Ecuador y El Salvador, además de la segunda en países como Honduras, Venezuela, Nicaragua, Cuba y Costa Rica, y ocupa el #1-2 en México. Además, tiene presencia Chile, Panamá, República Dominicana, Bolivia, Paraguay y Puerto Rico.</p>
      <p>Allworks ayuda a las personas a encontrar un trabajo mejor y a crecer profesionalmente y a las empresas a encontrar al profesional que mejor encaje con sus necesidades. Con este objetivo ofrece también soluciones innovadoras para la gestión del talento y la digitalización del reclutamiento.</p>
    <br>
    <h2>Fundado en 2020</h2>
    <p>Allworks fue creada en el año 2020 en Cancún, México, donde ha tenido su sede durante estos años.</p>
    </div>
  </div>
</div>
<br><br>
<h1 class="text-center">Fundadores:</h1>
<br><br>
<div class="container">
  <div class="row">
    <div class="col text-center">
    <a href="https://www.facebook.com/chistian.tunpoot"><img class="foto" src="https://i.ibb.co/ZdyM9YW/118152527-1742814125856445-1495926890310847759-n.jpg" alt="118152527-1742814125856445-1495926890310847759-n" border="0"></a>
    <br><br>
    <h4>Christian Tun</h4></a>
    </div>
    <div class="col order-5 text-center">
    <a href="https://www.facebook.com/enrique.nohun"><img class="foto" src="https://i.ibb.co/bmZSd0s/151740244-3750083988437388-1922168511999467789-n.jpg" alt="151740244-3750083988437388-1922168511999467789-n" border="0"></a>
    <br><br>
    <h4>Enrique Noh</h4></a>
    </div>
    <div class="col order-1 text-center">
    <a href="https://www.facebook.com/edgardavid.mezquitauc"><img class="foto" src="https://i.ibb.co/n8zrPtd/161807782-2459795884152584-86933492174497847-n.jpg" alt="161807782-2459795884152584-86933492174497847-n" border="0"></a>
    <br><br>
    <h4>Edgar Mezquita</h4></a>
    </div>
    <div class="col order-1 text-center">
    <a href="https://www.facebook.com/ricardo.cobachan/"><img class="foto" src="https://i.ibb.co/nmFZQVS/Whats-App-Image-2021-03-05-at-1-59-51-AM.jpg" alt="Whats-App-Image-2021-03-05-at-1-59-51-AM" border="0"></a>
    <br><br>
    <h4>Ricardo Cobá</h4></a>
    </div>
  </div>
</div>


@endsection
