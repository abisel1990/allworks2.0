<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Empresa\EmpleoController;
use App\Models\Empresa;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/nosotros', [App\Http\Controllers\HomeController::class, 'nosotros'])->name('nosotros');
Route::post('/tags', [App\Http\Controllers\BuscarEmpleoController::class, 'buscar'])->name('tags');
Route::get('/buscarpor', [App\Http\Controllers\BuscarEmpleoController::class, 'buscarpor'])->name('buscarpor');


Auth::routes();
// Route::middleware(['auth','verified'])->group(function(){
//     Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
// });

// Auth::routes();

Route::middleware(['empresa', 'verified'])->group(function () {

    //rutas

    Route::get('/bienvenido', [App\Http\Controllers\Empresa\EmpresaController::class, 'index'])->name('bienvenido');
    Route::get('/contacto', [App\Http\Controllers\Empresa\EmpresaController::class, 'contacto'])->name('contacto');
    Route::get('/empresa', [App\Http\Controllers\Empresa\PublicacionesController::class, 'publicaciones'])->name('empresa');
    Route::post('/empresa', [App\Http\Controllers\Empresa\PublicacionesController::class, 'destroy'])->name('empresa');
    Route::delete('empresa/{id}',[App\Http\Controllers\Empresa\PublicacionesController::class, 'destroy'])->name('empresa.destroy');


    //prueba de rutas
    Route::get('/empleo', [App\Http\Controllers\Empresa\EmpleoController::class, 'crearEmpleo'])->name('empleo');
    Route::post('/guardar', [App\Http\Controllers\Empresa\EmpleoController::class, 'store'])->name('guardar');
    // Route::post('/empleo', [App\Http\Controllers\Empresa\EmpleoController::class, 'destroy'])->name('empleo');
    Route::get('/verdetalles/{id}', [App\Http\Controllers\Empresa\EmpleoController::class, 'show'])->name('verdetalles/{id}');


    Route::get('/perfil', [App\Http\Controllers\Empresa\PerfilEmpController::class, 'perfil'])->name('perfil');
    Route::get('/registro', [App\Http\Controllers\Empresa\PerfilEmpController::class, 'registro'])->name('registro');
    Route::post('/store', [App\Http\Controllers\Empresa\PerfilEmpController::class, 'store'])->name('store');
    Route::get('/perfil/edit', [App\Http\Controllers\Empresa\PerfilEmpController::class, 'edit'])->name('perfil.edit');
    Route::post('/perfil/actualizar', [App\Http\Controllers\Empresa\PerfilEmpController::class, 'actualizar'])->name('perfil.actualizar');
});

Route::middleware(['postulante', 'verified'])->group(function () {
    Route::get('/empleos', [App\Http\Controllers\Postulante\PostulacionesController::class, 'postulaciones'])->name('empleos');
    Route::get('/postularme/{id}/{empresa}', [App\Http\Controllers\Postulante\PostulacionesController::class, 'postularme'])->name('postularme/{id}/{empresa}');
    // Route::get('/postulante', [App\Http\Controllers\Postulante\BuscarController::class, 'index'])->name('postulante');
    Route::get('/postulaciones', [App\Http\Controllers\Postulante\PostulanteController::class, 'index'])->name('postulaciones');
    Route::get('/contactoPos', [App\Http\Controllers\Postulante\PostulanteController::class, 'contactoPos'])->name('contactoPos');
    Route::get('/buscarempleo', [App\Http\Controllers\BuscarEmpleoController::class, 'buscarempleo'])->name('buscarempleo');


    Route::get('/perfilPostulante', [App\Http\Controllers\Postulante\PerfilPosController::class, 'perfil'])->name('perfilPostulante');
    Route::get('/registroPostulante', [App\Http\Controllers\Postulante\PerfilPosController::class, 'registro'])->name('registroPostulante');
    Route::post('/storePostulante', [App\Http\Controllers\Postulante\PerfilPosController::class, 'store'])->name('storePostulante');
    Route::get('/perfil/editPostulante', [App\Http\Controllers\Postulante\PerfilPosController::class, 'edit'])->name('perfil.editPostulante');
    Route::post('/perfil/actualizarPostulante', [App\Http\Controllers\Postulante\PerfilPosController::class, 'actualizar'])->name('perfil.actualizarPostulante');
});
